# PPW Starter Pack

Gue asumsikan saat mengikuti tutorial ini kalian udah:

1. Install Python (versi nya sesuai sama yang diminta dosen)
2. Ngedaftarin Python ke Environment Variables (cara ngeceknya, ketik aja python di Command Prompt, keluar gak)

#### **Step 1: Install pip (package manager)**

1. Buka link ini --> [get-pip.py](https://bootstrap.pypa.io/get-pip.py) , copas kode yang ada di situ ke sebuah file Python baru, namain file nya **get-pip.py**

2. buka cmd, masuk ke folder tempat lo naro file tadi, ketik:

   ```bash
   python get-pip.py
   ```
   kalo **python** gabisa, cobain **py**, dan coba command nya:

   ```bash
   python/py (pilih salah satu) -m pip
   ```

3. Tunggu sampe pip selesai di instal. Buat ngecek, ketik:

   ```bash
   pip
   ```
   atau pake cara kedua:

   ```bash
   python/py (pilih salah satu) -m pip
   ```

4. Kalo keluar list command dari pip itu, berarti **pip sudah terinstall.**


**Untuk Linux/Mac:**

Ketik perintah tsb di terminal:

```bas
sudo apt install python3-pip
```

#### **Step 2: Nyiapin repository buat ppw**

1. Bikin repository baru di akun gitlab kalian. Namanya bebas, tapi direkomendasikan **ppw-lab**

2. Bedakan antara **origin** sama **upstream**.

   - Origin: Repo yang lo bikin di akun gitlab lo, buat naro hasil tugas lab lo
   - Upstream: Repo asli PPW yang dibikin sama dosen, buat tempat lo nge pull soal-soal lab baru

3. Konfigurasi awal repo ppw:

   - Clone dari repo bikinan dosen

     ```bash
     git clone https://gitlab.com/PPW-2017/ppw-lab
     ```

   - Masuk ke folder **ppw-lab** hasil clone tadi.

   - Hapus dulu remote ke origin

     ```bash
     git remote remove origin
     ```

   - Buka repo gitlab kalian, klik icon copy disini:

     ![Screenshot from 2018-09-04 14-41-45](https://gitlab.com/izznfkhrlislm/ppw-starterpack/raw/master/Screenshot%20from%202018-09-04%2014-41-45.png)

   - Add remote origin pake link yang kalian copas tadi

     ```bash
     git remote add origin [link yang lo copy tadi]
     ```

   - Add remote upstream ke repo PPW bikinan dosen tadi

     ```bash
     git remote add upstream https://gitlab.com/PPW-2017/ppw-lab.git
     ```

#### **Step 3: Konfigurasi virtualenv**

1. Install virtualenv

   ```bash
   pip install virtualenv
   ```
   atau pake cara kedua kalau cara diatas gabisa.

   ```bash
   python/py (pilih salah satu) -m pip install virtualenv
   ```

   **Untuk Linux:** install virtualenv dengan cara

   ```bash
   sudo pip3 install virtualenv
   ```

2. Bikin virtualenv nya (dalam contoh ini nama folder nya 'venv')

   ```bash
   virtualenv venv
   ```
   atau pake cara kedua kalau cara diatas gabisa.

   ```bash
   python/py (pilih salah satu) -m venv venv
   ```

   

3. Aktifin virtualenv nya

   ```bash
   \venv\Scripts\activate.bat
   ```
   **Untuk Linux:**

   ```bash
   source venv/bin/activate
   ```

4. Masuk ke folder ppw-lab lo, ketik

   ```bash
   pip install -r requirements.txt
   ```
   atau

   ```bash
   python/py (pilih salah satu) -m pip install -r requirements.txt
   ```

5. Tunggu instalasi selesai

6. Udah bisa kerjain file nya.

#### **Step 4: Udah kelar ngerjain lab**

1. Add file nya

   ```bash
   git add .
   ```

2. Commit file nya sesuai apa yang lo kerjain

   ```bash
   git commit -m <message>
   ```

3. Push kerjaan lo, tapi push nya ke **origin**

   ```bash
   git push origin master
   ```

#### **SOP TIAP MAU NGERJAIN LAB**

1. Selalu pull kodingan terbaru dari repo bikinan dosen

   ```bash
   git pull upstream master
   ```

2. Aktifin virtualenv nya

3. Selalu install requirement, kali aja ada yang baru di file nya

   ```bash
   pip install -r requirements.txt
   ```



#### **Good Luck~**



